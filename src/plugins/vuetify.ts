import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify, {
    theme: {
        // primary: {base: '#10627A', darken1: '#455066'},
        primary: { base: '#0072BB', darken1: '#004774' },
        secondary: '#FFA500',
        // accent: '#4395AD',
        // info: '#4395AD',
        accent: '#7DA7D9',
        info: '#7DA7D9',
        success: '#FFFFFF',
        warning: '#FFC107',
        error: '#FF5252',
    },
    iconfont: 'md',
});
